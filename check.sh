#!/bin/bash

###############################################
# mailto: ggerman@gmail.com
# checkurls
# http://www.eosweb.info
# require curl
###############################################

url() {
  cat urls.csv | 
  replace  | 
  show
}

replace() {
  tr ',' ' '
}

show() {
  awk '{print $1}'
}

url | \
while read CMD; do
  echo $CMD
  curl -Is $CMD | head -n 1
done

